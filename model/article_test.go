package model

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCanCreateArticle(t *testing.T) {
	// Given
	name := "Ikan Asin"
	slug := "ikan-asin"
	body := "Kasus ikan asin menjadi viral"

	// When
	got := CreateArticle(name, slug, body)

	// Then
	assert.Equal(t, name, got.Name)
	assert.NotEmpty(t, got.CreatedAt)
}
