package model

import "time"

// Article store an article
type Article struct {
	Name      string
	Slug      string
	Body      string
	CreatedAt time.Time
}

// CreateArticle create an article
func CreateArticle(name, slug, body string) Article {
	return Article{
		Name:      name,
		Slug:      slug,
		Body:      body,
		CreatedAt: time.Now(),
	}
}
